package com.lato;

import org.springframework.context.support.ClassPathXmlApplicationContext;

public class AnnotationsBeanScopesApp {

    public static void main(String[] args) {
        ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext("applicationContext.xml");

        Coach theCoach = context.getBean("tennisCoach", Coach.class);

        Coach theSecondCoach = context.getBean("tennisCoach", Coach.class);

        boolean result = (theCoach == theSecondCoach);

        System.out.println("Is the same? " + result);

        System.out.println("Memory location coach " + theCoach);

        System.out.println("Memory location second " + theSecondCoach);


        context.close();

    }

}
