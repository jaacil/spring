package com.lato;

import org.springframework.stereotype.Component;

import java.util.Random;

@Component
public class RandomFortuneService implements FortuneService {

    // array of strings
    private final String[] fortunes = {
            "Beware of the woolf in sheeps clothes",
            "Diligence is the mother of good luck",
            "The journey is the reward"
    };

    private final Random myRandom = new Random();

    @Override
    public String getFortune() {
        int num = myRandom.nextInt(fortunes.length);
        return fortunes[num];
    }

}
