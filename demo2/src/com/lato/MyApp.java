package com.lato;

public class MyApp {

	public static void main(String[] args) {

		// create the object
		Coach theCoach = new com.lato.TrackCoach(() -> "ITS YOUT LUCKY DAY");
		
		// use the object
		System.out.println(theCoach.getDailyWorkout());		
	}

}
