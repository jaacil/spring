package com.lato.demo;

import com.lato.entity.Instructor;
import com.lato.entity.InstructorDetail;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;


public class Create {

    public static void main(String[] args) {

        SessionFactory factory = new Configuration()
                .configure("hibernate.cfg.xml")
                .addAnnotatedClass(Instructor.class)
                .addAnnotatedClass(InstructorDetail.class)
                .buildSessionFactory();

        Session session = factory.getCurrentSession();

        try {
            Instructor instructor = new Instructor("Kamil", "Komar", "KK@email.pl");

            InstructorDetail instructorDetail = new InstructorDetail("Komarov", "Gaming");

            instructor.setInstructorDetail(instructorDetail);

            session.beginTransaction(); //start transaction

            session.save(instructor); //this also save details object because of cascadetype = all

            session.getTransaction().commit(); //commit transaction

            System.out.println("Done.");

        } finally {
            factory.close();
        }

    }

}
