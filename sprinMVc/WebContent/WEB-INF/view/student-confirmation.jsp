<%--
  Created by IntelliJ IDEA.
  User: Jacek
  Date: 22.07.2021
  Time: 19:23
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<html>
<head>
    <title>Student confirmation</title>
</head>
<body>

Student is confirmed: ${student.firstName} ${student.lastName}
<br>
Country: ${student.country}
<br>
Favourite language is: ${student.favouriteLanguage}
<br>
Operating systems:
<ul>
    <c:forEach var="temp" items="${student.operatingSystem}">
        <li> ${temp} </li>
    </c:forEach>
</ul>

</body>
</html>
