package com.lato.demo;

import com.lato.entity.Instructor;
import com.lato.entity.InstructorDetail;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;


public class DeleteInstructorDetails {

    public static void main(String[] args) {

        SessionFactory factory = new Configuration()
                .configure("hibernate.cfg.xml")
                .addAnnotatedClass(Instructor.class)
                .addAnnotatedClass(InstructorDetail.class)
                .buildSessionFactory();

        Session session = factory.getCurrentSession();

        try {

            session.beginTransaction(); //start transaction
            int id = 3;

           InstructorDetail instructorDetail = session.get(InstructorDetail.class, id);

            System.out.println("Instructor details: " + instructorDetail.toString());

            System.out.println("Associated instructor: " + instructorDetail.getInstructor().toString());

            System.out.println("Deleting instructorDetail " + instructorDetail.toString());

            //remove the associated object reference
            //break bi-directional link

            instructorDetail.getInstructor().setInstructorDetail(null);

            session.delete(instructorDetail); //cascade delete - with instructor



            session.getTransaction().commit(); //commit transaction

            System.out.println("Done.");

        } catch (NullPointerException e) {
            System.out.println("Null!");

//            e.printStackTrace();
        } finally {
            session.close();
            factory.close();
        }

    }

}
