package com.lato;

public class TrackCoach implements Coach {

	private FortuneService fortuneService;

	public TrackCoach(FortuneService fortuneService) {
		this.fortuneService = fortuneService;
	}

	@Override
	public String getDailyWorkout() {
		return "Run a hard 5k";
	}

	@Override
	public String getDailyFortune() {
		return "Just do it: " + fortuneService.getFortune();
	}

	//initialization method
	public void initMethod() {
		System.out.println(getClass().getSimpleName() + ": inside init method");
	}

	//destroy method
	public void destroyMethod() {
		System.out.println(getClass().getSimpleName() + ": inside destroy method");
	}

}










