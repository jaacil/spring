package com.lato.demo;

import com.lato.entity.Student;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;


public class UpdateStudentDemo {

    public static void main(String[] args) {

        SessionFactory factory = new Configuration()
                .configure("hibernate.cfg.xml")
                .addAnnotatedClass(Student.class)
                .buildSessionFactory();

        Session session = factory.getCurrentSession();

        try {

            int studentId = 1;

            session.beginTransaction(); //start transaction

            System.out.println("Getting student with id: " + studentId);

            Student student = session.get(Student.class, studentId);
            System.out.println("Student : " + student.toString());

            System.out.println("Updating name.");
            student.setFirstName("Spock");

            System.out.println("Updating email");
            session.createQuery("update Student set email='foooo@o2.pl' where id=" + studentId).executeUpdate();

            session.getTransaction().commit();

            System.out.println("Done.");

        } finally {
            factory.close();
        }

    }

}
