package com.lato.demo;

import com.lato.entity.Student;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

public class PrimaryKeyDemo {

    public static void main(String[] args) {
        SessionFactory factory = new Configuration()
                .configure("hibernate.cfg.xml")
                .addAnnotatedClass(Student.class)
                .buildSessionFactory();

        Session session = factory.getCurrentSession();

        try {

            System.out.println("Creating 3 students");
            Student student = new Student("Jane", "Doe", "Jane@o2.pl");
            Student student1 = new Student("Jan", "Kowalski", "Jan@o2.pl");
            Student student2 = new Student("Walter", "White", "Walter@o2.pl");

            session.beginTransaction();

            session.save(student);
            session.save(student1);
            session.save(student2);

            session.getTransaction().commit();
        } finally {
            session.close();
        }

    }

}
