package com.lato.demo;

import com.lato.entity.Course;
import com.lato.entity.Instructor;
import com.lato.entity.InstructorDetail;
import com.lato.entity.Review;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;


public class CreateReviewsandCourses {

    public static void main(String[] args) {

        SessionFactory factory = new Configuration()
                .configure("hibernate.cfg.xml")
                .addAnnotatedClass(Instructor.class)
                .addAnnotatedClass(InstructorDetail.class)
                .addAnnotatedClass(Course.class)
                .addAnnotatedClass(Review.class)
                .buildSessionFactory();

        Session session = factory.getCurrentSession();

        try {
            int id = 1;

            session.beginTransaction(); //start transaction

            Course course = new Course("Java Spring");

            course.add(new Review("Great course, love it"));
            course.add(new Review("Super"));
            course.add(new Review("What a dumb course"));

            System.out.println("Saving the course");
            System.out.println(course);
            System.out.println(course.getReviewList());

            session.save(course);

            session.getTransaction().commit(); //commit transaction

            System.out.println("Done.");

        } finally {
            session.close();
            factory.close();
        }

    }

}
