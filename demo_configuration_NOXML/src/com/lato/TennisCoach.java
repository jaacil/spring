package com.lato;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;

@Component
@Scope("singleton")
public class TennisCoach implements Coach {

    @Autowired
    @Qualifier("happyFortuneService")
    private FortuneService fortuneService;

    //init method
    @PostConstruct
    public void init() {
        System.out.println("Initialization");
    }

    //destroy method
    @PreDestroy
    public void destroy() {
        System.out.println("Destroying");
    }


    //wstrzykiwanie konstruktora
//    @Autowired
//    public TennisCoach(FortuneService fortuneService) {
//        this.fortuneService = fortuneService;
//    }

    public TennisCoach() {
        System.out.println("object constructed");
    }

    //wstrzykiwanie settera
//    @Autowired
    public void setFortuneService(FortuneService fortuneService) {
        System.out.println("setter injected");
        this.fortuneService = fortuneService;
    }

    //wstrzykiwanie metody
//    @Autowired
    public void doSmth(FortuneService fortuneService) {
        System.out.println("method injected");
        this.fortuneService = fortuneService;
    }

    @Override
    public String getDailyWorkout() {
        return "Practice your backhand volley";
    }

    @Override
    public String getDailyFortune() {
        return fortuneService.getFortune();
    }

}
