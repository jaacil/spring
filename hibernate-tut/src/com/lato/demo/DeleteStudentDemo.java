package com.lato.demo;

import com.lato.entity.Student;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;


public class DeleteStudentDemo {

    public static void main(String[] args) {

        SessionFactory factory = new Configuration()
                .configure("hibernate.cfg.xml")
                .addAnnotatedClass(Student.class)
                .buildSessionFactory();

        Session session = factory.getCurrentSession();

        try {

            int studentId = 2;

            session.beginTransaction(); //start transaction

            System.out.println("Getting student with id " + studentId) ;

            Student myStudent = session.get(Student.class, studentId);

            System.out.println("Deleting student with id " + studentId) ;
//            session.delete(myStudent); //deleting by method

            studentId = 3;

            session.createQuery("delete from Student where id=" + studentId).executeUpdate(); //deleting by query

            session.getTransaction().commit(); //commit transaction

            System.out.println("Done.");

        } finally {
            factory.close();
        }

    }

}
