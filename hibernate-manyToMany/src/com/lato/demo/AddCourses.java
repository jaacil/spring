package com.lato.demo;

import com.lato.entity.*;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;


public class AddCourses {

    public static void main(String[] args) {

        SessionFactory factory = new Configuration()
                .configure("hibernate.cfg.xml")
                .addAnnotatedClass(Instructor.class)
                .addAnnotatedClass(InstructorDetail.class)
                .addAnnotatedClass(Course.class)
                .addAnnotatedClass(Review.class)
                .addAnnotatedClass(Student.class)
                .buildSessionFactory();

        Session session = factory.getCurrentSession();

        try {
            int id = 2;

            session.beginTransaction(); //start transaction

            Student student = session.get(Student.class, id);

            Course course = new Course("SQL for Beginner");
            Course course2 = new Course("HTML 5");
            Course course3 = new Course("Java FX");

            course.addStudent(student);
            course2.addStudent(student);
            course3.addStudent(student);

            session.save(course);
            session.save(course2);
            session.save(course3);

            session.getTransaction().commit(); //commit transaction

            System.out.println("Done.");

        } finally {
            session.close();
            factory.close();
        }

    }

}
