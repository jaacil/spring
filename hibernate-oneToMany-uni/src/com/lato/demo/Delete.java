package com.lato.demo;

import com.lato.entity.Instructor;
import com.lato.entity.InstructorDetail;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;


public class Delete {

    public static void main(String[] args) {

        SessionFactory factory = new Configuration()
                .configure("hibernate.cfg.xml")
                .addAnnotatedClass(Instructor.class)
                .addAnnotatedClass(InstructorDetail.class)
                .buildSessionFactory();

        Session session = factory.getCurrentSession();

        try {

            session.beginTransaction(); //start transaction

            int id = 2;

            Instructor instructor = session.get(Instructor.class, id);

            if(instructor != null) {
                session.delete(instructor); //also delete details - because of cascade
            }

            session.getTransaction().commit(); //commit transaction

            System.out.println("Done.");

        } finally {
            factory.close();
        }

    }

}
