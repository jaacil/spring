package com.lato;

import org.springframework.stereotype.Component;
import org.w3c.dom.stylesheets.LinkStyle;

import java.io.*;
import java.lang.reflect.Field;
import java.nio.Buffer;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

@Component
public class SmthFortuneService implements FortuneService {

    private final List<String> list = new ArrayList();
    private final Random myRandom = new Random();

    public SmthFortuneService() {
        Path path = Paths.get("src/fortunes.txt");
        File file = new File(path.toString());

        try (BufferedReader bufferedReader = new BufferedReader(new FileReader(file))) {
            String line = "";

            while ((line = bufferedReader.readLine()) != null) {
                list.add(line);
            }

        } catch (IOException e1) {
            e1.printStackTrace();
        }
    }

    @Override
    public String getFortune() {
        int index = myRandom.nextInt(list.size());
        return list.get(index);
    }
}
