package com.lato.demo;

import com.lato.entity.Course;
import com.lato.entity.Instructor;
import com.lato.entity.InstructorDetail;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import org.hibernate.query.Query;


public class FetchJoin {

    public static void main(String[] args) {

        SessionFactory factory = new Configuration()
                .configure("hibernate.cfg.xml")
                .addAnnotatedClass(Instructor.class)
                .addAnnotatedClass(InstructorDetail.class)
                .addAnnotatedClass(Course.class)
                .buildSessionFactory();

        Session session = factory.getCurrentSession();

        try {
            int id = 1;

            session.beginTransaction(); //start transaction

            Query<Instructor> query = session.createQuery("select i from  Instructor i "
                            + "JOIN FETCH i.courseList "
                            + "where i.id=:instructorId",
                    Instructor.class);

            query.setParameter("instructorId", id);

            Instructor instructor = query.getSingleResult();

            System.out.println("jacek: Instructor " + instructor.toString());

            session.getTransaction().commit(); //commit transaction

            session.close();

            System.out.println("jacek: Session is now closed.");

            System.out.println("jacek: Courses: " + instructor.getCourseList());

            System.out.println("jacek: Done.");

        } finally {
            session.close();
            factory.close();
        }

    }

}
