package com.lato.demo;

import com.lato.entity.Student;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;


public class CreateStudentDemo {

    public static void main(String[] args) {

        SessionFactory factory = new Configuration()
                .configure("hibernate.cfg.xml")
                .addAnnotatedClass(Student.class)
                .buildSessionFactory();

        Session session = factory.getCurrentSession();

        try {
            System.out.println("Creating a student object");
            Student student = new Student("Jacek", "Lato", "email@o2.pl");

            session.beginTransaction(); //start transaction

            System.out.println("Saving student");
            session.save(student); //save

            session.getTransaction().commit(); //commit transaction

            System.out.println("Done.");

        } finally {
            factory.close();
        }

    }

}
