package com.lato.demo;

import com.lato.entity.Instructor;
import com.lato.entity.InstructorDetail;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;


public class GetInstructorDetails {

    public static void main(String[] args) {

        SessionFactory factory = new Configuration()
                .configure("hibernate.cfg.xml")
                .addAnnotatedClass(Instructor.class)
                .addAnnotatedClass(InstructorDetail.class)
                .buildSessionFactory();

        Session session = factory.getCurrentSession();

        try {

            session.beginTransaction(); //start transaction
            int id = 222;

           InstructorDetail instructorDetail = session.get(InstructorDetail.class, id);

            System.out.println("Instructor details: " + instructorDetail.toString());

            System.out.println("Associated instructor: " + instructorDetail.getInstructor().toString());

            session.getTransaction().commit(); //commit transaction

            System.out.println("Done.");

        } catch (NullPointerException e) {
            System.out.println("Null!");

//            e.printStackTrace();
        } finally {
            session.close();
            factory.close();
        }

    }

}
