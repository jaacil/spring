package com.lato;

public class CricketCoach implements Coach {

    private FortuneService fortuneService;
    private String email;
    private String team;

    public CricketCoach() {
        System.out.println("Cricket Coach Constructor");
    }

    @Override
    public String getDailyWorkout() {
        return "Practice fast bowling for 15 minutes";
    }

    @Override
    public String getDailyFortune() {
        return fortuneService.getFortune();
    }

    public void setFortuneService(FortuneService fortuneService) {
        System.out.println("Cricket Coach Fortune Setter");
        this.fortuneService = fortuneService;
    }

    public void setEmail(String email) {
        System.out.println("Cricket Coach Email Setter");
        this.email = email;
    }

    public void setTeam(String team) {
        System.out.println("Cricket Coach Team Setter");
        this.team = team;
    }

    public String getEmail() {
        return email;
    }

    public String getTeam() {
        return team;
    }
}
