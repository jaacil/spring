package com.lato;

import java.util.Random;

public class HappyFortuneService implements FortuneService {

    private final String[] fortuneArr = {
            "Today is your lucky day",
            "HURRAAAY",
            "YOU WON, dont worry be happy"
    };

    @Override
    public String getFortune() {

        int rand = new Random().nextInt(fortuneArr.length);
        return fortuneArr[rand];
    }
}
