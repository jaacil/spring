package com.lato.demo;

import com.lato.entity.Course;
import com.lato.entity.Instructor;
import com.lato.entity.InstructorDetail;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;


public class EagerLazy {

    public static void main(String[] args) {

        SessionFactory factory = new Configuration()
                .configure("hibernate.cfg.xml")
                .addAnnotatedClass(Instructor.class)
                .addAnnotatedClass(InstructorDetail.class)
                .addAnnotatedClass(Course.class)
                .buildSessionFactory();

        Session session = factory.getCurrentSession();

        try {
            int id = 1;

            session.beginTransaction(); //start transaction

            Instructor instructor = session.get(Instructor.class, id);

            System.out.println("jacek: Instructor " + instructor.toString());

            System.out.println("jacek: Courses: " + instructor.getCourseList());

            session.getTransaction().commit(); //commit transaction

            System.out.println("jacek: Done.");

        } finally {
            session.close();
            factory.close();
        }

    }

}
