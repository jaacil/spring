package com.lato.demo;

import com.lato.entity.*;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;


public class CreateCourseAndStudents {

    public static void main(String[] args) {

        SessionFactory factory = new Configuration()
                .configure("hibernate.cfg.xml")
                .addAnnotatedClass(Instructor.class)
                .addAnnotatedClass(InstructorDetail.class)
                .addAnnotatedClass(Course.class)
                .addAnnotatedClass(Review.class)
                .addAnnotatedClass(Student.class)
                .buildSessionFactory();

        Session session = factory.getCurrentSession();

        try {
            int id = 1;

            session.beginTransaction(); //start transaction

            Course course = new Course("Java Spring");

            System.out.println("Saving the course");
            session.save(course);
            System.out.println("Saved course: " + course);

            Student s1 = new Student("Jacek", "Lato", "email@email.com");
            Student s2 = new Student("John", "Doe", "JD@email.com");

            course.addStudent(s1);
            course.addStudent(s2);

            System.out.println("Saving students: \n");
            session.save(s1);
            session.save(s2);

            System.out.println("Saved students: " + course.getStudentList());

            session.getTransaction().commit(); //commit transaction

            System.out.println("Done.");

        } finally {
            session.close();
            factory.close();
        }

    }

}
