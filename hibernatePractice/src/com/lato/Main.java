package com.lato;

import com.lato.entity.Employee;
import com.sun.xml.bind.v2.runtime.output.SAXOutput;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

import java.util.List;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) {

        int choose = 0;
        Scanner scanner = new Scanner(System.in);
        boolean exit = false;

        try {
            while (!exit) {

                System.out.println("Choose action\n" +
                        "0: show employees\n" +
                        "1: add new employee\n" +
                        "2: delete employee\n" +
                        "3: update employee\n" +
                        "4: exit");

                choose = scanner.nextInt();

                String name, lastName, company;
                int id;

                switch (choose) {
                    case 0:
                        System.out.println("Showing employees");
                        showEmployees();
                        break;
                    case 1:
                        System.out.println("Enter name: ");
                        name = scanner.next();
                        System.out.println("Enter last name: ");
                        lastName = scanner.next();
                        System.out.println("Enter company: ");
                        company = scanner.next();

                        System.out.println("Adding employee");
                        addNewEmployee(name, lastName, company);
                        break;
                    case 2:
                        System.out.println("Enter employee id to delete: ");
                        id = scanner.nextInt();
                        deleteEmployee(id);
                        break;
                    case 3:
                        System.out.println("Enter employee id to update: ");
                        id = scanner.nextInt();
                        System.out.println("Enter new name: ");
                        name = scanner.next();
                        System.out.println("Enter new last name: ");
                        lastName = scanner.next();
                        System.out.println("Enter new company: ");
                        company = scanner.next();
                        updateEmployee(id, name, lastName, company);
                        break;
                    case 4: exit = true;
                        break;
                    default:
                        System.out.println("Default");
                        break;
                }
            }
        } finally {
            factory().close();
        }


    }

    public static SessionFactory factory() {
        SessionFactory factory = new Configuration()
                .configure("hibernate.cfg.xml")
                .addAnnotatedClass(Employee.class)
                .buildSessionFactory();

        return factory;
    }

    public static void showEmployees() {

        Session session = factory().getCurrentSession();

        session.beginTransaction();

        List<Employee> employees = session.createQuery("from Employee").getResultList();

        for(Employee employee : employees) {
            System.out.println(employee.toString());
        }

        session.getTransaction().commit(); //commit transaction
    }

    public static void addNewEmployee(String name, String lastName, String company) {

        Session session = factory().getCurrentSession();

        session.beginTransaction();

        Employee employee = new Employee(name, lastName, company);
        session.save(employee);

        session.getTransaction().commit();
    }

    public static void deleteEmployee(int id) {
        Session session = factory().getCurrentSession();

        session.beginTransaction();

        session.createQuery("delete from Employee where id=" + id).executeUpdate();

        session.getTransaction().commit();
    }

    public static void updateEmployee(int id, String name, String lastName, String company) {

        Session session = factory().getCurrentSession();

        session.beginTransaction();

        session.createQuery("update Employee set firstName='" + name + "', lastName='" + lastName + "', company='" + company + "' where id=" + id).executeUpdate();

        session.getTransaction().commit();
    }
}
