package com.lato.demo;

import com.lato.entity.Course;
import com.lato.entity.Instructor;
import com.lato.entity.InstructorDetail;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;


public class DeleteCourses {

    public static void main(String[] args) {

        SessionFactory factory = new Configuration()
                .configure("hibernate.cfg.xml")
                .addAnnotatedClass(Instructor.class)
                .addAnnotatedClass(InstructorDetail.class)
                .addAnnotatedClass(Course.class)
                .buildSessionFactory();

        Session session = factory.getCurrentSession();

        try {
            int id = 10;

            session.beginTransaction(); //start transaction

            Course course = session.get(Course.class, id);

            System.out.println("Deleting course: " + course.toString());

            session.delete(course);

            session.getTransaction().commit(); //commit transaction

            System.out.println("Done.");

        } finally {
            session.close();
            factory.close();
        }

    }

}
