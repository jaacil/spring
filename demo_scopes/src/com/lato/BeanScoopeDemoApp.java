package com.lato;

import org.springframework.context.support.ClassPathXmlApplicationContext;

public class BeanScoopeDemoApp {

    public static void main(String[] args) {
        ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext("beanScope-applicationContext.xml");
        Coach coach = context.getBean("myCoach", Coach.class);
        Coach secondCoach = context.getBean("myCoach", Coach.class);

        if(coach == secondCoach)
            System.out.println("The same");
        else
            System.out.println("Not the same");

        System.out.println("Memory location coach: " + coach);
        System.out.println("Memory location second coach: " + secondCoach);

        context.close();
    }
}
