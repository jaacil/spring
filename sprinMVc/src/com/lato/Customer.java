package com.lato;

import validation.CourseCode;

import javax.validation.constraints.*;

public class Customer {

    private String firstName;

    @NotNull(message="is required")
    @Size(min = 2, message="length must be at least 2 characters")
    private String lastName;

    @NotNull(message="is required")
    @Min(value = 1, message = "cant be less than 0")
    @Max(value = 10, message = "cant be higher than 10")
    private Integer freePasses;

    @Pattern(regexp = "^[0-9]{2}-[0-9]{3}", message = "Postal code must be XX-XXX and only digits")
    private String postalCode;

    @CourseCode(value = "Lato", message = "MUST START WITH Lato")
    private String courseCode;

    public String getCourseCode() {
        return courseCode;
    }

    public void setCourseCode(String courseCode) {
        this.courseCode = courseCode;
    }

    public String getPostalCode() {
        return postalCode;
    }

    public void setPostalCode(String postalCode) {
        this.postalCode = postalCode;
    }

    public Integer getFreePasses() {
        return freePasses;
    }

    public void setFreePasses(Integer freePasses) {
        this.freePasses = freePasses;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }
}
