package com.lato;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

@Component
public class DifferentCoach implements Coach {

    @Autowired
    @Qualifier("smthFortuneService")
    private FortuneService fortuneService;


    @Override
    public String getDailyWorkout() {
        return "DO SMTH";
    }

    @Override
    public String getDailyFortune() {
        return fortuneService.getFortune();
    }
}
