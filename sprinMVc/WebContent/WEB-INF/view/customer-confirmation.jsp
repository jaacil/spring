<%--
  Created by IntelliJ IDEA.
  User: Jacek
  Date: 26.07.2021
  Time: 16:00
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <title>Customer confirmation</title>
</head>
<body>
You are now registered.
<br><br>

<b>${customer.firstName} ${customer.lastName}

<br>
Free passes: ${customer.freePasses}
<br>
Postal code: ${customer.postalCode}
    <br>
    Course code: ${customer.courseCode}

</b>

</body>
</html>
