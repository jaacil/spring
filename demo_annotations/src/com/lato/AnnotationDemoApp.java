package com.lato;

import org.springframework.context.support.ClassPathXmlApplicationContext;

public class AnnotationDemoApp {

    public static void main(String[] args) {
        //read spring config
        ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext("applicationContext.xml");
        //get bean from spring container

        Coach theCoach = context.getBean("tennisCoach", Coach.class);
        Coach hokeyCoach = context.getBean("hokeyCoach", Coach.class);
        Coach diferentCoach = context.getBean("differentCoach", Coach.class);
        //call a method on the bean

        System.out.println(theCoach.getDailyWorkout());
        System.out.println(hokeyCoach.getDailyWorkout());

        //call method getDailyFortune

        System.out.println(theCoach.getDailyFortune());

        System.out.println(hokeyCoach.getDailyFortune());

        System.out.println("-----------------------");
        System.out.println(diferentCoach.getDailyFortune());

        //close
        context.close();

    }

}
