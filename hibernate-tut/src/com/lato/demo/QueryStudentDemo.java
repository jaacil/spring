package com.lato.demo;

import com.lato.entity.Student;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

import java.util.List;


public class QueryStudentDemo {

    public static void main(String[] args) {

        SessionFactory factory = new Configuration()
                .configure("hibernate.cfg.xml")
                .addAnnotatedClass(Student.class)
                .buildSessionFactory();

        Session session = factory.getCurrentSession();

        try {

            session.beginTransaction(); //start transaction


            List<Student> studentList = session.createQuery("from Student").getResultList();

            System.out.println("\nAll students");
            displayStudents(studentList);

            studentList = session.createQuery("from Student s where s.lastName ='White'").getResultList();

            System.out.println("\nStudents with last name White");
            displayStudents(studentList);

            studentList = session.createQuery("from Student s where s.lastName='White' OR s.firstName='Jacek'").getResultList();
            System.out.println("\nStudents with last name White OR first name Jacek");
            displayStudents(studentList);

            studentList = session.createQuery("from Student s where s.email like 'Jane%'").getResultList();
            System.out.println("\nStudents whom emails starts with Jane");
            displayStudents(studentList);

            session.getTransaction().commit(); //commit transaction

            System.out.println("Done.");

        } finally {
            factory.close();
        }

    }

    private static void displayStudents(List<Student> studentList) {
        for(Student student : studentList) {
            System.out.println(student.toString());
        }
    }

}
