package com.lato;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

@Component
public class HokeyCoach implements Coach {

//    @Autowired
    private FortuneService fortuneService;

    @Autowired
    public HokeyCoach(@Qualifier("randomFortuneService") FortuneService fortuneService) {
        this.fortuneService = fortuneService;
    }

    public HokeyCoach() {
        System.out.println("hokeyCoach construced");
    }

    @Override
    public String getDailyWorkout() {
        return "Hit the puck on the ice";
    }

    @Override
    public String getDailyFortune() {
        return fortuneService.getFortune();
    }
}
