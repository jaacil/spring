package com.lato.demo;

import com.lato.entity.*;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;


public class DeleteCourses {

    public static void main(String[] args) {

        SessionFactory factory = new Configuration()
                .configure("hibernate.cfg.xml")
                .addAnnotatedClass(Instructor.class)
                .addAnnotatedClass(InstructorDetail.class)
                .addAnnotatedClass(Course.class)
                .addAnnotatedClass(Review.class)
                .addAnnotatedClass(Student.class)
                .buildSessionFactory();

        Session session = factory.getCurrentSession();

        try {
            int id = 12;

            session.beginTransaction(); //start transaction

            Course course = session.get(Course.class, id);

            System.out.println("Found course: " + course + ". Deleting.");

            session.delete(course);

            session.getTransaction().commit(); //commit transaction

            System.out.println("Done.");

        } finally {
            session.close();
            factory.close();
        }

    }

}
