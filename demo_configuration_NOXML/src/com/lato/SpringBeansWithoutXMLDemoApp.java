package com.lato;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class SpringBeansWithoutXMLDemoApp {

    public static void main(String[] args) {
        //read spring config
        AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext(SportConfig.class);
        //get bean from spring container

        SwimCoach theCoach = context.getBean("swimCoach", SwimCoach.class);

        //call a method on the bean

        System.out.println(theCoach.getDailyWorkout());


        //call method getDailyFortune

        System.out.println(theCoach.getDailyFortune());

        System.out.println("Email: " + theCoach.getEmail());
        System.out.println("Team: " + theCoach.getTeam());

        System.out.println("-----------------------");

        //close
        context.close();

    }

}
