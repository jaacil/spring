<%--
  Created by IntelliJ IDEA.
  User: Jacek
  Date: 22.07.2021
  Time: 19:16
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<html>
<head>
    <title>Student registration form</title>
</head>
<body>

<form:form action="processForm" modelAttribute="student">
    First Name: <form:input path="firstName" />
    <br><br>

    Last Name: <form:input path="lastName" />
    <br><br>

    Country:
    <form:select path="country">
    <form:options items="${student.countryOptions}" />
<%--    <form:option value="Poland" label="Poland"/>--%>
<%--    <form:option value="Brazil" label="Brazil"/>--%>
<%--    <form:option value="Germany" label="Germany"/>--%>
<%--    <form:option value="USA" label="USA"/>--%>
    </form:select>
    <br><br>

    Java <form:radiobutton path="favouriteLanguage" value="Java" />
    PHP <form:radiobutton path="favouriteLanguage" value="PHP" />
    C++ <form:radiobutton path="favouriteLanguage" value="C++" />
    C# <form:radiobutton path="favouriteLanguage" value="C#" />
    <br><br>

    In what operating system you have experience?
    MS Windows <form:checkbox path="operatingSystem" value="MS Windows"/>
    Linux <form:checkbox path="operatingSystem" value="Linux"/>
    Mac OS <form:checkbox path="operatingSystem" value="Mac OS"/>
    <br><br>

    <input type="submit" value="Submit">
</form:form>

</body>
</html>
